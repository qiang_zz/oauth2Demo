package com.qz.authcode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.http.util.TextUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Hello world!
 *
 */
@SpringBootApplication
@Controller
public class App {
	public final static String clientId = "test";
	public final static String clientSecret = "test";
	public final static String scope = "write";
	public final static String redirectUrl = "http://localhost:9001/welcome";


	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}

	@GetMapping("/welcome")
	@ResponseBody
	public String welcome(String code,HttpServletResponse response) throws IOException {
		// 通过授权码获取token
		if (null != code && !"".equals(code)) {
			String result = getTokenByCode(code,response);
			if (!TextUtils.isEmpty(result)) {
				return result;
			}
		}else{
		
			response.sendRedirect("http://localhost:9000/oauth/authorize?response_type=code&authorized_grant_types=authorization_code&scope="
					+ scope + "&client_id=" + clientId + "&redirect_uri=" + redirectUrl);
		    return null;
		}
		return "武汉蓝泰源信息技术有限公司";
	}

	private String getTokenByCode(String code, HttpServletResponse response) {
		// 获取token后修改Authencation状态
		String result="";
		String url="http://localhost:9000/oauth/token";
		String body="client_id="+clientId+"&client_secret="+clientSecret+"&grant_type=authorization_code&code="+code+"&redirect_uri="+redirectUrl;
		System.out.println("body=============="+body);
		HttpClient httpClient = new HttpClient();
		httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(15000);
		PostMethod postMethod = new PostMethod(url);
		postMethod.getParams().setParameter(HttpMethodParams.SO_TIMEOUT, 60000);
		Header header = new Header("Content-Type", "application/x-www-form-urlencoded");
		postMethod.setRequestHeader(header);
		postMethod.setRequestBody(body);
		InputStream is = null;
		BufferedReader br = null;
		// 执行POST方法
        try {
            int statusCode = httpClient.executeMethod(postMethod);
            // 判断是否成功
            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("Method faild: " + postMethod.getStatusLine());
            }
            // 获取远程返回的数据
            is = postMethod.getResponseBodyAsStream();
            // 封装输入流
            br = new BufferedReader(new InputStreamReader(is, "UTF-8"));

            StringBuffer sbf = new StringBuffer();
            String temp = null;
            while ((temp = br.readLine()) != null) {
                sbf.append(temp).append("\r\n");
            }

            result = sbf.toString();
            System.err.println("result=========="+result);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 关闭资源
            if (null != br) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (null != is) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            // 释放连接
            postMethod.releaseConnection();
        }
        return result;
	}

}
